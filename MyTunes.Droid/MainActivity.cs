﻿using Android.App;
using Android.OS;
using System.Collections.Generic;

namespace MyTunes
{
	[Activity(Label = "@string/app_name", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : ListActivity
	{
		protected async override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			List<Song> data = await SongLoader.Load();

			ListAdapter = new ListAdapter<Song>() {
				DataSource = data,
				TextProc = s => s.Name,
				DetailTextProc = s => s.Artist + " - " + s.Album,

			};

//			ListAdapter = new ListAdapter<string>() {
//				DataSource = new[] { "One", "Two", "Three" }
//			};
		}
	}
}


