using UIKit;
using System.Collections.Generic;

namespace MyTunes
{
	public class MyTunesViewController : UITableViewController
	{
		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();

			if (UIDevice.CurrentDevice.CheckSystemVersion(7,0)) {
				TableView.ContentInset = new UIEdgeInsets (20, 0, 0, 0);
			}
		}

		public async override void ViewDidLoad()
		{
			base.ViewDidLoad();

//			TableView.Source = new ViewControllerSource<string>(TableView) {
//				DataSource = new string[] { "One", "Two", "Three" },
//			};

			List<Song> data = await SongLoader.Load();

			TableView.Source = new ViewControllerSource<Song>(TableView)
			{
				DataSource = data,
				TextProc = s => s.Name,
				DetailTextProc = s => s.Artist + " - " + s.Album,
			};
		}
	}

}

